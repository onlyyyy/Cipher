#ifndef COMMON_H
#define COMMON_H


#include<QModelIndex>
#include<QTreeView>
#include<QTextEdit>
#include<QFile>
#include<QString>
#include<QStringList>
#include<QList>
#include<QStandardItemModel>
#include<QStringLiteral>
#include<QStandardItem>
#include<QDebug>
#include<QCryptographicHash>
#include<QMessageBox>

#include<QFileDialog>

#include<QChar>
#include"playfair.h"
#include"hill.h"

#include"rsa.h"

#include"DES.h"

#endif // COMMON_H
