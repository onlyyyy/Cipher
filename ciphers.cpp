#include "ciphers.h"
#include "ui_ciphers.h"
#include"common.h"
Ciphers::Ciphers(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Ciphers)
{
    ui->setupUi(this);
    setFixedSize(800,600);

    CipherListInit();
    init_treeView();
    ui->treeView_cipherList->expandAll();//默认展开所有的节点
    ui->treeView_cipherList->setEditTriggers(nullptr); //不允许点击修改
    ui->stackedWidget_main->setCurrentWidget(ui->page_main);
}

Ciphers::~Ciphers()
{
    delete ui;
}

bool Ciphers::ifhasChinese(QString str)
{
    bool Flag = str.contains(QRegExp("[\\x4e00-\\x9fa5]+"));
    return Flag;
}



//退出程序
void Ciphers::on_pushButton_close_clicked()
{
    close();
}


void Ciphers::CipherListInit()
{
    typicalList<<"凯撒密码"<<"Playfair"<<"希尔密码";
    pair<<"DES"<<"3DES"<<"AES";
    nopair<<"RSA"<<"椭圆曲线(ECC)";
    MD<<"SHA-1"<<"SHA-256"<<"MD5";
    stream<<"RC4";

}

void Ciphers::init_treeView()
{
    QStandardItemModel * model = new QStandardItemModel(ui->treeView_cipherList);
    model->setHorizontalHeaderLabels(QStringList()<<"算法列表");

    QList<QStandardItem *> items1;

    QStandardItem * item1 = new QStandardItem("古典密码");
    for(int i = 0;i<typicalList.length();i++)
    {
        QList<QStandardItem *> typical;
        QStandardItem *itemtemp = new QStandardItem(typicalList[i]);
        typical.append(itemtemp);
        item1->appendRow(typical);
    }
    items1.append(item1);

    QList<QStandardItem *> items2;
    QStandardItem *item2 = new QStandardItem("对称加密");
    for(int i=0;i<pair.length();i++)
    {
        QList<QStandardItem *> pairs;
        QStandardItem *itemtemp = new QStandardItem(pair[i]);
        pairs.append(itemtemp);
        item2->appendRow(pairs);
    }
    items2.append(item2);

    QList<QStandardItem *> items3;
    QStandardItem *item3 = new QStandardItem("非对称加密");
    for(int i=0;i<nopair.length();i++)
    {
        QList<QStandardItem *> nopairs;
        QStandardItem *itemtemp = new QStandardItem(nopair[i]);
        nopairs.append(itemtemp);
        item3->appendRow(nopairs);
    }
    items3.append(item3);

    QList<QStandardItem *> items4;
    QStandardItem *item4 = new QStandardItem("摘要算法");
    for(int i=0;i<MD.length();i++)
    {
        QList<QStandardItem *> MDS;
        QStandardItem *itemtemp = new QStandardItem(MD[i]);
        MDS.append(itemtemp);
        item4->appendRow(MDS);
    }
    items4.append(item4);

    QList<QStandardItem *> items5;
    QStandardItem *item5 = new QStandardItem("流密码");
    for(int i=0;i<stream.length();i++)
    {
        QList<QStandardItem *>streams;
        QStandardItem *itemtemp = new QStandardItem(stream[i]);
        streams.append(itemtemp);
        item5->appendRow(streams);
    }
    items5.append(item5);
    model->appendRow(items1);
    model->appendRow(items2);
    model->appendRow(items3);
    model->appendRow(items4);
    model->appendRow(items5);
    ui->treeView_cipherList->setModel(model);
}

void Ciphers::on_treeView_cipherList_clicked(const QModelIndex &index)
{
    QString windowname;
    QModelIndex thisindex = ui->treeView_cipherList->currentIndex();
    if(thisindex.parent().isValid()==false)
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_main);
        return; //如果选的不是加密算法的类型，那就切换到主窗口
    }
    windowname = thisindex.data().toString();
    qDebug()<<windowname;
    if(windowname == "凯撒密码")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_Julius);
        ui->spinBox_Julius->setRange(-26,26);
        ui->textEdit_Julius_cipher->setReadOnly(true);

    }
    else if(windowname == "Playfair")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_playfair);

    }
    else if(windowname == "希尔密码")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_hill);
    }
    else if(windowname == "RSA")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_RSA);
    }
    else if(windowname == "MD5")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_MD5);
    }
    else if(windowname == "DES")
    {
        ui->stackedWidget_main->setCurrentWidget(ui->page_DES);
    }


}

void Ciphers::on_pushButton_Julius_do_clicked()
{
    QString entered = ui->textEdit_Julius_plain->toPlainText();
    if(entered.length()==0)
    {
        QMessageBox::warning(nullptr,"警告","要处理的文本不能为空");
        return;
    }
    if(ifhasChinese(entered)==true)
    {
        QMessageBox::warning(nullptr,"警告","暂不支持中文");
        return;
    }
    int index = ui->spinBox_Julius->value();
    int ascii = 0;

    for(int i=0;i<entered.length();i++)
    {
        ascii =int(entered.at(i).toLatin1());
        ascii += index;
        entered[i] = char(ascii);
    }
    ui->textEdit_Julius_cipher->setText(entered);
    ui->textEdit_Julius_cipher->repaint();
}



void Ciphers::on_pushButton_playfair_cipher_clicked()
{
    if(ui->lineEdit_playfair_cipher->text().length()==0
            ||ui->textEdit_playfair_plain->toPlainText().length()==0)
    {
        QMessageBox::warning(nullptr,"警告","密钥和明文不能为空");
        return;
    }

    QString key = ui->lineEdit_playfair_cipher->text();
    QString encryptText = ui->textEdit_playfair_plain->toPlainText();
    QString result;
    if(encryptText.length()>20)
    {
        QMessageBox::warning(nullptr,"警告","明文长度不应大于20");
        return;
    }
    if(hadInt(encryptText)==false||hadInt(key)==false)
    {
        QMessageBox::warning(nullptr,"警告","密钥和明文不能有数字");
        return;
    }
    qDebug()<<"密钥为"<<key;
    qDebug()<<"明文为"<<encryptText;
    result = encrypt(key,encryptText,0);
    qDebug()<<"密文为"<<result;
    if((encryptText.length()&1) != 0) //明文是奇数
    {
        ui->textEdit_playfair_cipher->setText(result.mid(0,result.length()-1));
    }
    else
    {
        ui->textEdit_playfair_cipher->setText(result);
    }

    ui->textEdit_playfair_cipher->repaint();
}

void Ciphers::on_pushButton_playfair_plain_clicked()
{
    if(ui->lineEdit_playfair_cipher->text().length()==0
            ||ui->textEdit_playfair_cipher->toPlainText().length()==0)
    {
        QMessageBox::warning(nullptr,"警告","密钥和密文不能为空");
        return;
    }
    QString key = ui->lineEdit_playfair_cipher->text();
    QString plainText = ui->textEdit_playfair_cipher->toPlainText();
    QString result;
    if(plainText.length()>20)
    {
        QMessageBox::warning(nullptr,"警告","密文长度不应大于20");

        return;
    }
    int flag = 0;
    if((plainText.length()&1)==1)
    {
        plainText += "A"; //密文不能是奇数，会出大问题
        flag = 1;
    }
    if(hadInt(plainText)==false||hadInt(key)==false)
    {
        QMessageBox::warning(nullptr,"警告","密钥和密文不能有数字");
        return;
    }
    result = decrypt(key,plainText,flag);

    ui->textEdit_playfair_plain->setText(result);

    ui->textEdit_playfair_plain->repaint();

}

void Ciphers::on_pushButton_hill_encrypt_clicked()
{
    QString plain = ui->lineEdit_hill_plain->text();
    if(plain.length()>3)
    {
        plain = plain.mid(0,3);
    }
    QString result = encrypt_hill(plain);

    ui->lineEdit_hill_cipher->setText(result);
    ui->lineEdit_hill_cipher->repaint();

}



void Ciphers::on_pushButton_RSA_encrypt_clicked()
{

    QString plain = ui->textEdit_RSA_plain->toPlainText();
    if(ifIsFullDigital(plain)==false)
    {
        QMessageBox::warning(nullptr,"警告","暂时只支持数字");
        return;
    }


    Initialize(plain);
    while(!e)
    {
        RSA_Initialize();
    }
    RSA_Encrypt();
    a = "";
    for(int i=0;i<1024 && Ciphertext[i]!=0;i++)
    {
        a += QString::number(Ciphertext[i]);

    }
    ui->textEdit_RSA_cipher->setText(a);
    ui->lineEdit_RSA_e->setText(QString::number(e));
    ui->lineEdit_RSA_d->setText(QString::number(d));
    ui->lineEdit_RSA_n->setText(QString::number(n));
    QString e,d,n;
    e=ui->lineEdit_RSA_e->text();
    d=ui->lineEdit_RSA_d->text();
    n=ui->lineEdit_RSA_n->text();

    word="公钥:e="+e+"  n="+n;
    ui->textEdit_RSA_cipher->repaint();
}



void Ciphers::on_pushButton_RSA_decrypt_clicked()
{

    QString b=ui->textEdit_RSA_cipher->toPlainText();
    if(ifIsFullDigital(b)==false)
    {
        QMessageBox::warning(nullptr,"警告","暂时只支持数字");
        return;
    }
    ui->textEdit_RSA_plain->setText(b);
    RSA_Decrypt();
    QString a="";
    //for(int i = 0; i<1024 && Ciphertext[i]!=0 ; i++)
    for(int i=1023;i>=0;i--)
    {
        if(Ciphertext[i]!=0)
        {
            a+=QString::number(Ciphertext[i]);
            //qDebug()<<a;
            //qDebug()<<Ciphertext[i];
        }
        else
        {
            a+="";
        }
    }
    ui->textEdit_RSA_cipher->setText(a);
    ui->page_RSA->repaint();
}


//生成文本的MD5
void Ciphers::on_pushButton_TextMD5_clicked()
{
    QString input=ui->textEdit_MD5_input->toPlainText();
    QString result;
    QCryptographicHash md(QCryptographicHash::Md5);
    QString md5;
    QByteArray bb,cc;
    cc=input.toLatin1();//转String
    bb = QCryptographicHash::hash(cc,QCryptographicHash::Md5 );
    result=bb.toHex();//转换存储方式
    md5.append(result);
    ui->textEdit_MD5_result->setText(result);
    ui->page_MD5->repaint();
}

//生成文件的MD5
void Ciphers::on_pushButton_FileMD5_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
            tr("选择要计算的文件"),
            "",
            "",
         0);
    ui->textEdit_MD5_input->setText(filename);
    QString path=ui->textEdit_MD5_input->toPlainText();
    QFile *myfile=new QFile(path);
    myfile->open(QIODevice::ReadOnly);
    QCryptographicHash md(QCryptographicHash::Md5);
    QString md5;
    QByteArray bb;
    bb = QCryptographicHash::hash(myfile->readAll(),QCryptographicHash::Md5 );

    md5.append(bb.toHex());
    md5.toStdString();
    myfile->close();
    ui->textEdit_MD5_result->setText(md5);
    ui->page_MD5->repaint();
}

void Ciphers::on_pushButton_DES_encrypt_clicked()
{
    QString key = ui->lineEdit_DES_key->text();
    QString plain = ui->textEdit_DES_plain->toPlainText();
    if(key.length() == 0||plain.length()==0)
    {
        QMessageBox::warning(nullptr,"警告","密钥和明文长度不能为0");
        return;
    }
    for(int i=0;i<8;i++)
    {
        MIN[i] = static_cast<int>(plain[i].toLatin1());
        MI[i] = static_cast<int>(key[i].toLatin1());
    }
    DES_Efun(MIN,MI,output);
    QString result;
    result = QString::number(output[0]);
    for(int i=1;i<64;i++)
    {
        result += QString::number(output[i]);
    }
    ui->textEdit_DES_cipher->setText(result);
    ui->page_DES->repaint();



}

void Ciphers::on_pushButton_DES_decrypt_clicked()
{
    QString key = ui->lineEdit_DES_key->text();
    QString cipher = ui->textEdit_DES_cipher->toPlainText();
    if(key.length() == 0||cipher.length()==0)
    {
        QMessageBox::warning(nullptr,"警告","密钥和明文长度不能为0");
        return;
    }
    for(int i=0;i<9;i++)
    {

        MI[i] = static_cast<int>(key[i].toLatin1());
    }
    DES_Dfun(output,MI,MIN);
    QString result = QString(QLatin1String(MIN));
    ui->textEdit_DES_plain->setText(result);
    ui->textEdit_DES_plain->repaint();

}

