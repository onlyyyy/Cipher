#ifndef CIPHERS_H
#define CIPHERS_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Ciphers; }
QT_END_NAMESPACE

class Ciphers : public QMainWindow
{
    Q_OBJECT

public:
    Ciphers(QWidget *parent = nullptr);
    ~Ciphers();

private slots:
    void on_pushButton_close_clicked();

    void CipherListInit(); //初始化支持的加解密算法

    void init_treeView(); //初始化treeview的结点

    void on_treeView_cipherList_clicked(const QModelIndex &index);


    //凯撒密码
    void on_pushButton_Julius_do_clicked();

    bool ifhasChinese(QString str);

    //Playfair
    void on_pushButton_playfair_cipher_clicked();

    void on_pushButton_playfair_plain_clicked();


    //判断QString是否是纯数字
    bool ifIsFullDigital(QString str)
    {
        bool flag = false;
        double d = -1;
        d = str.toDouble(&flag);
        return flag;
    }

    //判断QString是否含有数字
    bool hadInt(QString src)
    {
        bool flag = src.contains(QRegExp("^[A-Za-z]+$"));
        return flag;
    }
    void on_pushButton_hill_encrypt_clicked();

    void on_pushButton_RSA_encrypt_clicked();

    void on_pushButton_RSA_decrypt_clicked();

    void on_pushButton_TextMD5_clicked();

    void on_pushButton_FileMD5_clicked();

    void on_pushButton_DES_encrypt_clicked();

    void on_pushButton_DES_decrypt_clicked();

private:
    Ui::Ciphers *ui;

    QString  word; //RSA

    QList<QString> typicalList; //古典密码
    QList<QString> pair; //对称加密
    QList<QString> nopair; //非对称加密
    QList<QString> MD; //摘要算法
    QList<QString> stream; //流密码
};
#endif // CIPHERS_H
